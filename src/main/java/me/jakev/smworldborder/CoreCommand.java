package me.jakev.smworldborder;

import api.common.GameServer;
import api.listener.Listener;
import api.listener.events.player.PlayerChatEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.game.PlayerUtils;
import api.utils.game.SegmentControllerUtils;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.PlayerNotFountException;

import java.util.ArrayList;

/**
 * Created by Jake on 10/20/2020.
 * <insert description here>
 */
public class CoreCommand {
    public CoreCommand(StarMod mod){
        StarLoader.registerListener(PlayerChatEvent.class, new Listener<PlayerChatEvent>() {
            @Override
            public void onEvent(PlayerChatEvent event) {
                String txt = event.getText();
                PlayerState p = getPlayerFromName(event.getMessage().sender);
                if(txt.toLowerCase().startsWith("!core")) {
                    p.sendInventoryModification(p.getInventory().incExistingOrNextFreeSlot(ElementKeyMap.CORE_ID, 1), -9223372036854775808L);
                    p.sendInventoryModification(p.getInventory().incExistingOrNextFreeSlot(ElementKeyMap.REACTOR_MAIN, 1), -9223372036854775808L);
                    p.sendInventoryModification(p.getInventory().incExistingOrNextFreeSlot(ElementKeyMap.THRUSTER_ID, 1), -9223372036854775808L);
                    PlayerUtils.sendMessage(p, "There you go");
                }else if(txt.toLowerCase().startsWith("!recycle")){
                    PlayerControllable inside = PlayerUtils.getCurrentControl(p);
                    if(inside instanceof SegmentController) {
                        SegmentController root = ((SegmentController) inside).railController.getRoot();
                        int creds = (int) (root.railController.calculateRailMassIncludingSelf() * 1000);
                        for (SegmentController sc : SegmentControllerUtils.getAllDocksRecursive(new ArrayList<SegmentController>(), root)) {
                            sc.markForPermanentDelete(true);
                        }
                        p.modCreditsServer(creds);
                        PlayerUtils.sendMessage(p, "You are recieved: " + creds + " credits.");
                    }else{
                        PlayerUtils.sendMessage(p, "You are not in a ship.");
                    }
                }
            }
        }, mod);
    }
    public static PlayerState getPlayerFromName(String name){
        try {
            return GameServer.getServerState().getPlayerFromNameIgnoreCase(name);
        } catch (PlayerNotFountException e) {
            e.printStackTrace();
        }
        return null;
    }
}
