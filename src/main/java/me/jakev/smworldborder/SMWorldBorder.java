package me.jakev.smworldborder;

import api.ModPlayground;
import api.common.GameServer;
import api.listener.Listener;
import api.listener.events.block.SegmentPieceSalvageEvent;
import api.listener.events.entity.ShipJumpEngageEvent;
import api.listener.events.faction.SystemClaimEvent;
import api.listener.events.network.ServerPingEvent;
import api.listener.events.player.PlayerSpawnEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.StarRunnable;
import api.utils.VarUtil;
import api.utils.game.PlayerUtils;
import api.utils.game.SegmentControllerUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.controller.SectorSwitch;
import org.schema.game.server.data.PlayerNotFountException;

import java.io.IOException;
import java.util.ArrayList;

public class SMWorldBorder extends StarMod {
    public static void main(String[] args) { }
    @Override
    public void onGameStart() {
        setModName("SMWorldBorder");
        setModVersion("0.1");
        setModDescription("Limit reachable systems");
        setModAuthor("JakeV");
        setModSMVersion("0.202.101");
        setServerSide(true);
    }

    public static final Vector3i origin = new Vector3i(0,0,0);

    public static boolean tooFar(SegmentController controller){
        Vector3i system = controller.getSystem(new Vector3i());
        system.sub(origin);
        float dist = system.lengthSquared();
        return dist > systemRadius;
    }
    public static boolean tooFar(Vector3i system){
        return system.lengthSquared() > systemRadius;
    }
    public static int systemRadius = 2;
    public static final int creditsMax = 2000000000;

    @Override
    public void onEnable() {
        new SystemClaimOverhaul(this);
        new CoreCommand(this);
        systemRadius = getConfig("config").getConfigurableInt("Radius", 2);
        getConfig("config.yml").saveConfig();
        new StarRunnable(){
            @Override
            public void run() {
                ModPlayground.broadcastMessage("[Auto Broadcast] " + Util.getMessage());
            }
        }.runTimer(this, 25*120);
        StarLoader.registerListener(PlayerSpawnEvent.class, new Listener<PlayerSpawnEvent>() {
            @Override
            public void onEvent(final PlayerSpawnEvent event) {
                Vector3i sector = event.getSector();
                sector.sub(2,2,2);
                sector.lengthSquared();
                if(sector.lengthSquared() == 0){
                    int i = 23;
                    final Vector3i randomSector = new Vector3i(Util.randInt(-i, i),Util.randInt(-i, i),Util.randInt(-i, i));
                    new StarRunnable(){
                        @Override
                        public void run() {
                            GameServer.getServerState().getController()
                                    .queueSectorSwitch(
                                            event.getPlayer(),
                                            randomSector,
                                            SectorSwitch.TRANS_JUMP, false, true, true);
                        }
                    }.runLater(SMWorldBorder.this, 25*2);

                    PlayerState pl = event.getPlayer().getOwnerState();
                    PlayerUtils.sendMessage(pl, "Since you spawned in 2,2,2 I teleported you elsewhere. You're welcome!");
                }
            }
        }, this);
        StarLoader.registerListener(SegmentPieceSalvageEvent.class, new Listener<SegmentPieceSalvageEvent>() {
            @Override
            public void onEvent(SegmentPieceSalvageEvent event) {
                SegmentController segmentController = event.getSegmentController();
                ArrayList<PlayerState> ps = SegmentControllerUtils.getAttachedPlayers(segmentController);
                for (PlayerState p : ps) {
                    int addedCredits = 100;
                    try {
                        StellarSystem sys = GameServer.getUniverse().getStellarSystemFromStellarPos(segmentController.getSystem(new Vector3i()));
                        if (sys.getOwnerFaction() == p.getFactionId()) {
                            addedCredits *= 4;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    p.modCreditsServer(addedCredits);
                }
            }
        }, this);
        new StarRunnable(){
            @Override
            public void run() {
                if(GameServer.getServerState() != null){
                    for (PlayerState player : GameServer.getServerState().getPlayerStatesByName().values()) {
                        PlayerControllable currentControl = PlayerUtils.getCurrentControl(player);
                        if(currentControl instanceof SegmentController){
                            if(tooFar((SegmentController) currentControl)){
                                int timesWarned = VarUtil.incrementAndGet(player, "timesWarned");
                                if(timesWarned > 15){
                                    VarUtil.assignField(player, "timesWarned", 0);
                                    PlayerUtils.sendMessage(player, "Well. I warned you. Goodbye");
                                    PlayerUtils.sendMessage(player, "Pretend you just heard a cool explosion, unfortunately the modloader is not on release builds yet so I cant play it for you.");
                                    ((SegmentController) currentControl).startCoreOverheating(null);
                                    //goodbye retard!
                                    player.damage(100000, null, player);
                                }else{
                                    PlayerUtils.sendMessage(player, "!!! YOU HAVE LEFT THE WORLD BORDER !!!\n  >>> MOVE TOWARD 2,2,2 TO PREVENT BEING VAPORIZED");
                                }
                            }
                        }
                    }
                }
            }
        }.runTimer(this, 25*4);
        StarLoader.registerListener(ShipJumpEngageEvent.class, new Listener<ShipJumpEngageEvent>() {
            @Override
            public void onEvent(ShipJumpEngageEvent event) {
                if(event.isServer()) {
                    event.getController().sendServerMessage("Warp disrupted | FTL is disabled in this universe", 0);
                }
                event.setCanceled(true);
            }
        }, this);

        StarLoader.registerListener(ServerPingEvent.class, new Listener<ServerPingEvent>() {
            @Override
            public void onEvent(ServerPingEvent event) {
                event.setDescription(randomMotd());
                int players = event.getPlayers();
                if(players == 0){
                    players = (int) (Math.random()*3);
                }else if(players == 1){
                    players = 3;
                }else if(players > 1){
                    players *= 2;
                }
                event.setPlayers(players);
                event.setName(randomTitle());
            }
        }, this);

        StarLoader.registerListener(SystemClaimEvent.class, new Listener<SystemClaimEvent>() {
            @Override
            public void onEvent(SystemClaimEvent event) {
                event.setCanceled(true);
                try {
                    PlayerUtils.sendMessage(
                            GameServer.getServerState().getPlayerFromName(event.getOwnershipChange().initiator),
                            "System claiming is based on the total reactor level of all of your factions stations in the system. (HOME BASE NOT INCLUDED)");
                } catch (PlayerNotFountException e) {
                    e.printStackTrace();
                }
            }
        }, this);
    }
    public static String randomTitle(){
        switch ((int) (Math.random()*4)) {
            case 0:
                return "-STS- Modded StarMade (no crafting!) | discord.gg/n86mgD5";
            case 1:
                return "-STS- Modded StarMade (BEAM LEAD FIX) | discord.gg/n86mgD5";
            case 2:
                return "-STS- Modded StarMade (spawn w/ creds) | discord.gg/n86mgD5";
            case 3:
                return "-STS- Modded StarMade ( mine for credits ) | discord.gg/n86mgD5";
            default:
                return "-STS- Modded StarMade (error) | discord.gg/n86mgD5";
        }
    }
    public static String randomMotd(){
        switch ((int) (Math.random()*6)){
            case 0: return "Does LvD have a random MOTD? I don't think so.";
            case 1: return "JOIN THIS SERVER RIGHT NOW!!!!!!!!!!!!";
            case 2: return "StarMade Universe Update - But theres no universe, and theres no update";
            case 3: return "Did you know: StarMade might be going open source!";
            case 4: return "Also join our discord: discord.gg/n86mgD5";
            case 5: return "Property of the UwU empire";
            default: return "Ok so basically";
        }
    }
}
